from django.db import migrations
from tqdm import tqdm

from mapper.city_place_data import *


CITIES_TO_ADD = {
    'london': {'country': 'GB', 'verbose_name': 'London'},
    'washington_dc': {'country': 'US', 'verbose_name': 'Washington'},
    'glasgow': {'country': 'GB', 'verbose_name': 'Glasgow'},
}


def forwards_func(apps, schema_editor):
    PlaceData = apps.get_model("mapper", "PlaceData")
    db_alias = schema_editor.connection.alias

    for city, info in tqdm(CITIES_TO_ADD.items(), total=len(CITIES_TO_ADD), desc='Dump city data'):
        for records in getattr(globals()[city], 'add_{}'.format(city))():
            PlaceData.objects.using(db_alias).bulk_create(
                PlaceData(**{**vals, 'source': city, 'country_code': info['country']}) for vals in records
            )
        # Delete GeoNames record if city data successfully inserted.
        if records:
            try:
                geonames_place = (
                    PlaceData.objects.filter(name=info['verbose_name'], country_code=info['country'], source='geonames')
                        .order_by('-population')
                        .first()
                )
                geonames_place.delete()
            except Exception:
                pass


def reverse_func(apps, schema_editor):
    PlaceData = apps.get_model("mapper", "PlaceData")
    db_alias = schema_editor.connection.alias

    for city in CITIES_TO_ADD.keys():
        PlaceData.objects.using(db_alias).filter(source=city).delete()


class Migration(migrations.Migration):
    dependencies = [("mapper", "0002_dump_geonames")]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
