from django.contrib import admin
from mapper.models import SearchResults
from mapper.models import PlaceData

admin.site.register(SearchResults)
admin.site.register(PlaceData)
